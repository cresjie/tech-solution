@extends('layouts.main')

@section('stylesheets')
	<link rel="stylesheet" href="{{ asset('css/contentcss/home.css') }}">
@endsection

@section('scripts')
	<script src="{{ asset('js/contentjs/admin.js') }}"></script>
@endsection

@section('content')
	<div class="container" ng-controller="MainController">
		<h1>Tech Solution</h1>

		<div class="photos-container">

			<div class="row photo-list">
				<div class="col-md-3" ng-repeat="photo in photosResponse.data" ng-cloak>
					<div class="photobox">
						<a href="javascript:;" class="photo-equalizer" ng-click="preview(photo)">
							<img class="photo img-responsive" ng-src="@{{photo.url}}">

							<div class="overlay">
								<i class="glyphicon glyphicon-eye-open preview-icon"></i>
							</div>
						</a>
					</div>
				</div>
				
			</div>

			<div class="photo-ajax-loader">
				<div class="windows8">
					<div class="percentage"></div>
					<div class="wBall" id="wBall_1">
						<div class="wInnerBall"></div>
					</div>
					<div class="wBall" id="wBall_2">
						<div class="wInnerBall"></div>
					</div>
					<div class="wBall" id="wBall_3">
						<div class="wInnerBall"></div>
					</div>
					<div class="wBall" id="wBall_4">
						<div class="wInnerBall"></div>
					</div>
					<div class="wBall" id="wBall_5">
						<div class="wInnerBall"></div>
					</div>
				</div>
			</div>

			
			

		</div>

		<div class="form-inline" ng-cloak>
			<div class="form-group">
				<label>Page: </label> 
				<input type="number" min="1" max="@{{photosResponse.last_page}}" ng-model="filters.page" ng-change="changePage()" style="width: 3em">
				<span>of @{{photosResponse.last_page}}</span>
			</div>
		</div>

		<div id="photo-preview-modal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Photo Information</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<img class="img-responsive" ng-src="@{{previewPhoto.url}}">
						</div>
						

						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control" readonly ng-model="previewPhoto.name">
						</div>

						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control" readonly ng-model="previewPhoto.title">
						</div>

						<div class="form-group">
							<label>Description</label>
							<textarea class="form-control" rows="5" readonly ng-model="previewPhoto.description"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>



	</div>
@endsection