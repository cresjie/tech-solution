@extends('layouts.main')

@section('stylesheets')
	<link rel="stylesheet" href="{{ asset('css/contentcss/home.css') }}">
@endsection

@section('scripts')
	
	<!-- <script src="{{ asset('js/library/filewizard.js/FileUploader.js') }}"></script> -->
	<script src="{{ asset('js/library/filewizard.js/FileWizard-bundle.min.js') }}"></script>
	<script src="{{ asset('js/contentjs/home.js') }}"></script>
@endsection

@section('content')
	
	<div class="container" ng-controller="MainController">
		<h1>Tech Solution</h1>
		<div class="photos-container">

			<div class="row photo-list">
				<div class="col-md-3" ng-repeat="photo in photos" ng-cloak>
					<div class="photobox">
						<a href="javascript:;" class="photo-equalizer" ng-click="preview(photo)">
							<img class="photo img-responsive" ng-src="@{{photo.url}}">

							<div class="overlay">
								<i class="glyphicon glyphicon-eye-open preview-icon"></i>
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="filewizard text-center">
						<i class="glyphicon glyphicon-picture image-icon"></i>
						<h4>Drag & Drop <br>
							Or <br>
							Click to Add Image <br>
						</h4>

					</div>	
				</div>
			</div>

			<img id="test" src="">
		</div>

		<div id="photo-modal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Photo Information</h4>
					</div>
					<form class="photo-form" action="POST" ng-submit="submit($event)">
						<div class="modal-body">
							<div class="form-group">
								<img class="photo-preview img-responsive">
							</div>
							
							<div class="form-group">
								<label>Name</label>
								<input type="text" name="name" class="form-control" placeholder="Name" ng-model="newPhoto.name">
							</div>

							<div class="form-group">
								<label>Title</label>
								<input type="text" name="title" class="form-control" placeholder="Title" ng-model="newPhoto.title">
							</div>

							<div class="form-group">
								<label>Description</label>
								<textarea class="form-control" name="Description" rows="5" ng-model="newPhoto.description"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary btn-upload">Upload</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>

			</div>
		</div>

		<div id="photo-preview-modal" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Photo Information</h4>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<img class="img-responsive" ng-src="@{{previewPhoto.url}}">
						</div>
						

						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control" readonly ng-model="previewPhoto.name">
						</div>

						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control" readonly ng-model="previewPhoto.title">
						</div>

						<div class="form-group">
							<label>Description</label>
							<textarea class="form-control" rows="5" readonly ng-model="previewPhoto.description"></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	
	


@endsection