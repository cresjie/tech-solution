<!DOCTYPE html>
<html>
<head>
	@yield('meta-data')
	<title>Tech Solutions</title>

	@section('main-stylesheets')
		<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('js/library/bsbox/bsbox.css') }}">
		<link rel="stylesheet" href="{{ asset('css/main.css') }}">
		<style>*[ng-cloak]{display: none !important;}</style>
	@show
	@yield('stylesheets')
	
	@yield('head-script')
</head>
<body>

	@yield('content')
	
	@section('main-scripts')
		<script src="{{ asset('js/library/jquery/jquery-3.2.1.min.js') }}"></script>
		<script src="{{ asset('js/library/angular/angular.min.js') }}"></script>
		<script src="{{ asset('js/library/bootstrap/bootstrap.min.js') }}"></script>
		<script src="{{ asset('js/library/bsbox/bsbox2-bundle.min.js') }}"></script>
		<script src="{{ asset('js/main.js') }}"></script>
		<script>
			var url = {
				home: '{{URL::to('/')}}', 
				api: '{{URL::to('api')}}'
			},
			_token = '{{csrf_token()}}';
		</script>
	@show

	<div class="page-loader">
		<div class="windows8">
			<div class="percentage"></div>
			<div class="wBall" id="wBall_1">
				<div class="wInnerBall"></div>
			</div>
			<div class="wBall" id="wBall_2">
				<div class="wInnerBall"></div>
			</div>
			<div class="wBall" id="wBall_3">
				<div class="wInnerBall"></div>
			</div>
			<div class="wBall" id="wBall_4">
				<div class="wInnerBall"></div>
			</div>
			<div class="wBall" id="wBall_5">
				<div class="wInnerBall"></div>
			</div>
		</div>
	</div>
	@yield('scripts')
</body>
</html>