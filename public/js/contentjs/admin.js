(function(){
	var app = angular.module('App',[]);

	app.controller('MainController', function($scope, $http, $q){
		$scope.previewPhoto = {};

		$scope.photosResponse = {};

		$scope.filters = {
			page: 1,
			limit: 3
		};

		var photoQueues = [];

		$scope.cancelPhotoQueues = function(){
			photoQueues.forEach(function(q){
				q.resolve();
			})
		}

		$scope.loadPhotos = function(){

			$scope.cancelPhotoQueues();

			var q = $q.defer();
			
			$('.photos-container .photo-ajax-loader').show();


			$http.get(url.api + '/photos?' + $.param($scope.filters), {timeout: q.promise})
				.then(function(xhr){
					if(xhr.data.last_page < $scope.filters.page) {
						$scope.filters.page = xhr.data.last_page;
						$scope.loadPhotos();
					} else {
						$scope.photosResponse = xhr.data
					}

					
				})
				.finally(function(){
					$('.photos-container .photo-ajax-loader').fadeOut();
				});
			photoQueues.push(q);
		}
		$scope.loadPhotos();

		$scope.changePage = function(){
			$scope.loadPhotos();
		}

		$scope.preview = function(photo){
			angular.extend($scope.previewPhoto, photo);

			$('#photo-preview-modal').modal('show')
		}


	});//end MainController


	app.filter('range', function() {
	  return function(input, total) {
	    total = parseInt(total);

	    for (var i=0; i<total; i++) {
	      input.push(i);
	    }

	    return input;
	  };
	});



	angular.element(document).ready(function(){
		angular.bootstrap(document, ['App']);
	});
	


})();