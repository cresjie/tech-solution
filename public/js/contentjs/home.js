(function(){
	var app = angular.module('App',[]);

	app.controller('MainController', function($scope, $http){

		$scope.newPhoto = {
			_token: _token
		};
		$scope.previewPhoto = {};
		$scope.photos = [
			
		];

		var filewizardRef;

		var fwBox = new FileWizard('.filewizard',{
			paramName: 'photo',
			multipleFiles: false,
			url: url.api + '/photos',
			fileAdded: function(file){
				filewizardRef = fwBox;
				
				var fileReader = new FileReader;
				
				fileReader.onload = function(e) {
					$('#photo-modal .photo-preview').attr('src', this.result);
				}

				fileReader.readAsDataURL(file);
				$('#photo-modal').modal('show');
			},

			progress: function(percent){
				$('.page-loader .percentage').html(percent.toFixed(2) + '%');
			},

			success: function(response){
				
				if(response.success) {
					$scope.photos.push(response.data);
					$scope.newPhoto = {};
					$('#photo-modal .photo-preview').attr('src', '');

					$('#photo-modal').modal('hide');
					$scope.$apply();

				} else {
					bsbox.alert(parseError(response.error_msg)).on('hidden.bs.modal', function(){
						$('body').addClass('modal-open')
					})
				}
				
				console.log(1)
			},
			complete: function(){
				$('.page-loader').fadeOut();
			}
		});


		$scope.preview = function(photo){
			angular.extend($scope.previewPhoto, photo);

			$('#photo-preview-modal').modal('show')
		}

		$scope.submit = function(e){
			e.preventDefault();

			var validationPass = true;

			$('.photo-form .form-control').each(function(i, el){
				if(!$(el).val()) {
					bsbox.alert( 'The ' + $(el).attr('name') + ' is required.').on('hidden.bs.modal', function(){
						$('body').addClass('modal-open')
					});
					validationPass = false;
					return false;
				}
			});

			if(!validationPass) {
				return ;
			}

			$('.page-loader').show();

			filewizardRef.send($scope.newPhoto);
		}
		

	}); //end MainController


	angular.element(document).ready(function(){
		angular.bootstrap(document, ['App']);
	});
	

})()
