<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Response;


use App\Models\Photo;

class PhotoController extends Controller
{
    //

    public function index(Request $request)
    {
    	return Photo::with('user')
    				->orderBy('id', 'desc')
    				->paginate($request->input('limit', 15))
    				->appends($request->all());
    }

    public function store(Request $request)
    {
    	
    	$validator = Validator::make($request->all(), ['photo' => 'required|image']);
    	if($validator->fails()) {
    		return Response::json(['success' => false, 'error_msg' => $validator->messages()]);
    	}

    	if(!$request->photo->isValid()) {
    		return Response::json(['success' => false, 'error_msg' => 'Photo is invalid.']);
    	}

    	$filename = uniqid() . "." . $request->photo->extension();

    	/**
    	 * move the image to the storage path
    	 */
    	$request->photo->storeAs('photos', $filename, 'public');

    	$photo = new Photo($request->all());
    	$photo->filename = $filename;

    	if($photo->save()) {
    		return Response::json(['success' => true, 'data' => $photo]);
    	}

    	return Response::json(['success' => false, 'error_msg' => 'Unable to save photo.']);

    }
}
