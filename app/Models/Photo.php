<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Config;

class Photo extends Model
{
    protected $fillable = ['title', 'description', 'name'];


    public static function createRules()
    {
    	return [
    		'title' => 'required',
    		'description' => 'required'
    	];
    }
    
    /**
     * Accessor
     */

    public function getUrlAttribute($value)
    {
    	if($this->filename) {
    		return Config::get('filesystems.disks.public.url') . "/photos/{$this->filename}";
    	}
    }

    public function toArray()
    {
    	
    	$attrs = parent::toArray();
    	$attrs['url'] = $this->url;

    	return $attrs;
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
